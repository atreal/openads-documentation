.. _frontend_scss:

######################
Personnaliser le style
######################

Depuis sa version 6.0.0, openADS utilise Sass et la librairie d'icone Remixicon.
Dans la suite de cette section, nous expliquerons comment utiliser ces éléments afin de personnaliser openADS

Avant de commencer
##################

Il est important d'installer les dépendances Node.js (voir :ref:`Pré-requis<installation_prérequis>` ).
Pour se faire, à la racine de votre projet, lancer la commande suivante



.. code-block:: bash

    composer install


Cette commande permet d'initialiser toutes les dépendances Node.js, de copier les librairies nécessaires ainsi que que compiler le SCSS pré-existant.

Dans le cas où vous ne souhaitez pas installer Composer sur votre poste, il faudra lancer les commandes présentes le long de ce tutoriel, à commencer par la commande suivante:

.. code-block:: bash

    npm ci

Cette commande installe les dépendances renseignées dans le fichier ``./package-lock.json`` . 
Cette étape est indispensable pour manipuler le style de l'application, et avoir accès à la librairie d'icône Remixicon.

Il est également possible d'ajouter d'autres paquets Node.JS, en fonction des besoins de votre projet. Si de nouvelles dépendances sont ajoutées dans ``package.json``, il faudra au prélable lancer la commande suivante: 

.. code-block:: bash

    npm i


Modifier la feuille de style avec Sass
######################################

Compiler le SCSS
----------------
Un nouveau répertoire `app/scss` comporte l'ensemble du style qui affecte l'apparence de l'application. Afin d'assurer le bon fonctionnement de l'application, à la première utilisation, il sera nécessaire de compiler le scss grace à la commande suivante: 

.. code-block:: bash

    npm run build


Dans le cas où l'on souherait compiler le scss en continu, sans relancer la commande de compilation à chaque modification pendant 
le temps du développement, il faut lancer la commande suivante : 

.. code-block:: bash

    npm run watch

Cette commande "observe" chaque modification d'un fichier .scss et recompile tous les fichiers de style.




Écrire le style de l'application
--------------------------------
Le style est écrit en SCSS depuis la version 6.0.0 d'openADS. 

Pour modifier le style, il faudra modifier les fichiers .scss en fonction de vos besoins.

.. note::  
    Il existe deux type de fichiers .scss : 
        * **_nomDeFichier.scss** : Préfixé par un tiret bas, il s'agit d'un *fichier partiel*. Il faut alors l'importer dans le fichier
          ``app/scss/style.scss``.
        * **nomDeFichier.scss** : Non préfixé par un tiret bas, lorsque le style sera compilé, il créera un fichier ``nomDeFichier.css`` 
          dans le dossier ``app/css``. Il faudra alors l'importer comme une nouvelle feuille de style dans ``om_layout_jqueryui.class.php`` dans 
          ``var $html_head_css``.


Les fichiers sont organisés dans le dossier ``app/scss`` dans des répertoires spécifiques: 
    * **components/**:  Les components sont des éléments fondamentaux de l'application. Ce sont des éléments spécifiques affichés dans l'application (ex: Portlet, Menu, Icons, [...]). Dans ce dossier, on retrouve un répertoire *openads* comportant des components spécifiques à openADS (tasks, workflow, ...)
    * **design-system/**: Dans ce répertoire ce trouvent des éléments plus générique, pouvant être utilisé à travers différentes applications openMairie. Il n'y a normalement pas de besoin d'en modifier le contenu. Si c'était le cas, il faudrait donc faire une demande dans les autres applications openMairie afin d'y appliquer les mêmes modifications.
    * **openads/**: Ce dossier contient les fichiers partiels pour chaque entrée de menu de l'application openADS. Par exemple: Si l'on souhaite faire une modification sur la page des autorisations, il faudra donc modifier le fichier ``_autorisation.scss``
    * **widgets/**: Ce répertoire contient les fichiers scss spécifiques à la mise en page des widgets présent sur le tableau de bord de l'application.

Dans chacun de ces dossiers, on retrouve un fichier scss partiel dont le nom est le même que le nom du répertoire dans lequel sont importés tous les fichiers partiels présents dans le répertoire en question. Si l'on souhaite rajouter un nouveau répertoire, il faudra créer ce fichier également.
Par exemple : Dans le nouveau dossier ``new-repertoire`` devra contenir le fichier ``_new-repertoire.scss``. Il faudra ensuite importer ``_new-repertoire.scss`` dans ``style.scss`` à la racine de ``./app/scss``


Ajouter des icônes
##################

Une nouvelle librairie d'icone est utilisée dans openADS : `Remixicon <https://remixicon.com/>`_.

Cette librairie permet l'utilisation d'icone au format vectoriel en rajoutant une classe dans une balise html dans le code, sans avoir à rajouter de fichiers supplémentaire dans ``./app/img/`` en plusieurs formats pour avoir plusieurs tailles.

Pour ajouter une icone, aller sur le site de `Remixicon <https://remixicon.com/>`_, choisir l'icone souhaitée en cliquant dessus. Une pop-up apparait: 

.. image:: ../_static/remixicon.png

Il faudra donc copier le code html 

.. code-block:: HTML

    <i class="ri-arrow-up-line"></i> 
    
et le coller à l'endroit souhaité dans le projet.

Les icones fonctionnent de la même manière qu'une police d'écriture, et peuvent être stylisées avec les mêmes propriétés css : *font-size*, *color*, *font-weight*,...


Écrire du SCSS
##############

Écrire du SCSS comporte quelques subtiles différences comparé à du CSS. Afin de mieux comprendre comment rédiger du SCSS, des tutoriels et des explications sont disponibles sur la `documentation officielle du SCSS <https://sass-lang.com/documentation/>`_