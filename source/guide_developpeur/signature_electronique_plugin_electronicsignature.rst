.. _signature_electronique_plugin_electronicsignature:

############################
Plugin 'electronicsignature'
############################


Ce connecteur permet  et le logiciel openADS.


Description de la configuration du connecteur electronicsignature
#################################################################

La configuration du plugin est définie dans dyn/electronicsignature.inc.php.

Modèle de fichier de configuration :

.. code:: php

    $conf["electronicsignature-default"] = array (
        "unexpected_collectivite" => array(),
        "1" => array(
            'connector' => '', 
            'path' => '', 
            'url_base' => 'https://localhost/', 
            'debug' => false, 
            'insecure' => false, 
        ),
    );


Liste des paramètres obligatoires : 

- **connector** Nom du connecteur
- **path** Chemin du répertoire contenant la classe du connecteur
- **url_base** URL de base de l'API
- **debug** Active ou désactive la journalisation des requêtes sortantes et des retours (true | false)
- **insecure** Active ou désactive la vérification du certificat SSL/TLS lors de la connexion au serveur distant (true | false)

Paramètres à saisir dans le signataire arrêté :

Le champ se nomme "Paramètre du parapheur", dans le cas d'un connecteur Pastell iParapheur il faut saisir ce paramètre :

.. code-block:: json

      {"circuit" : "SIGNATURE"}

Le circuit peut être différent en fonction du signataire.

Pour plus d'informations concernant le fonctionnement dans openADS, voir la partie :ref:`Paramétrage d'un signataire arrêté <parametrage_signataire_arrete>` et la partie :ref:`Envoi du document en signature sur le parapheur<instruction_parapheur>`.

********
Méthodes
********

send_for_signature()
********************

::

    send_for_signature(array $data, string $file_content, array $dossier_metadata, array $optional_data = null)

Permet à openADS d'envoyer un document d'instruction en signature dans le parapheur.

Paramètres
``````````
(array)  $data             Liste des paramètres génériques du connecteur.

(string) $file_content     Contenu du document à signer.

(array)  $dossier_metadata Métadonnées du dossier d'instruction.

(array)  $optional_data    Paramètres spécifique au connecteur.

Retour
``````
(array)  Tableau de résultat retourné par le connecteur parapheur, sinon retourne une exception.

get_signature_status()
**********************

::

    get_signature_status(array $data)

Permet à openADS de récupérer le statut de signature du parapheur.

Paramètres
``````````
(array)  $data Tableau contenant l'identifiant du parapheur.

Retours
```````
(array)  Tableau de résultat retourné par le connecteur parapheur, sinon retourne une exception.

cancel_send_for_signature()
***************************

::

    cancel_send_for_signature(array $data)

Permet à openADS d'annuler l'envoi en signature au parapheur.

Paramètres
``````````
(array)  $data Tableau contenant l'identifiant du parapheur.

Retours
```````
(array)  Tableau de résultat retourné par le connecteur parapheur, sinon retourne une exception.

get_signed_document()
*********************

::

    get_signed_document(array $data)

Permet à openADS de récupérer le document signé présent dans le parapheur.

Paramètres
``````````
(array)  $data Tableau contenant l'identifiant du parapheur.

Retours
```````
(array)  Tableau de résultat retourné par le connecteur parapheur, sinon retourne une exception.

query_db()
*********************

::

    query_db(array $data)

Permet au parapheur de consulter la base de données.

Paramètres
``````````
(string)  $query_sql SELECT SQL autorisé sur la base de données.
(boolean)  $force_return Nécessaire au fonctionnement de la requête.

Retours
```````
(array)|(boolean)  Tableau de résultat retourné au connecteur parapheur par la base de données, sinon retourne une booléen False.

