.. _connexion:

#########
Connexion
#########

Se connecter à l'application openADS
####################################

Ouvrir le navigateur et renseigner l'URL (adresse web) de l'application openADS.


Page d'authentification
***********************

La page d'authentification s'affiche (si vous ne vous êtes pas encore authentifié).

Sur celle-ci vous pouvez constater:

- deux champs "Identifiant" et "Mot de passe"
- un bouton "Se connecter"
- un lien "Mot de passe oublié ?" (si votre configuration le permet)
- un lien "Se connecter avec OpenID Connect (SSO)" (si votre configuration le permet)
- la version d'openADS (en pied de page)

Vous devez avoir un compte en base de données de l'application pour pouvoir vous connecter.

.. Note:: Il y a donc une opération de "provisioning" nécessaire qui consiste à créer les comptes
          des utilisateurs (manuellement ou de manière automatisée) avant toute connexion possible.

.. Note:: Lors d'une installation dans le cadre d'un développement ou d'un test, le compte
          administrateur est déjà créé. Vous trouverez les informations
          :ref:`ici<installation_login>`.


Identifiant / Mot de passe
**************************

En remplissant ces champs l'application va vous authentifier en fonction des données présentes dans
sa base de données.

Annuaire LDAP
=============

Si l'utilisateur associé à l'identifiant saisi provient d'un annuaire *LDAP* alors l'application
openADS devra être connectée à cet annuaire *LDAP* pour réaliser l'authentification, qu'elle lui
délègue.

.. Note:: Un problème réseau ou de configuration peut donc empêcher l'authentification de
          l'utilisateur lorsqu'il y a un système d'annuaire impliqué.


OpenID Connect (SSO)
********************

Un *SSO* (*Single Sign On*) est un système qui offre à l'utilisateur la possibilité de
s'authentifier une seule fois sur une seule application puis que toutes les autres applications
(qui supportent ce système) le "reconnaisse" automatiquement sans lui redemander de s'authentifier.
Ce système peut être mis en oeuvre par divers protocoles et l'un de ces protocoles qui est supporté
par openADS est *OpenID Connect*.

Ainsi, si la configuration de l'application openADS le permet, un lien est affiché
"Se connecter avec OpenID Connect (SSO)". C'est une méthode d'authentification alternative qui ne
nécessite pas d'avoir un mot de passe dans openADS (mais un compte avec un identifiant oui).

Lorsque l'utilisateur clique sur ce lien, cela le redirige vers une application tierce à laquelle
openADS délègue l'authentification de l'utilisateur. Ce dernier devra avoir un compte sur cette
application tierce, et renseigner, par exemple, un identifiant et un mot de passe pour celle-ci.

.. Note:: L'identifiant (et le mot de passe) sur l'application tierce est potentiellement
          complètement différent de l'identifiant dans openADS

Après avoir été authentifié sur cette application tierce, l'utilisateur est à nouveau redirigé
vers openADS. L'application "reconnait" alors l'utilisateur sans lui demander plus d'information
(il n'est pas nécessaire de réaliser plusieurs authentifications).


Rester connecté
###############

Les informations de connexion à l'application openADS sont stockées dans un cookie de session du
navigateur. Cela permet de rester connecter à l'application.

Il y a néanmoins une limite (qui dépend de la configuration de l'application et du serveur) au-delà
de laquelle ces informations sont considérées comme obsolètes, et il faudra alors s'authentifier à
nouveau. C'est ce qu'on appelle la limite de durée de la session.

.. Note:: Puisque les informations de connexion sont stockées dans un cookier dans votre navigateur
          cela signifie que vous ne pouvez pas être reconnu dans un autre navigateur, ou que si vous
          supprimez tous les cookies de votre navigateur vous devrez vous authentifier à nouveau.
