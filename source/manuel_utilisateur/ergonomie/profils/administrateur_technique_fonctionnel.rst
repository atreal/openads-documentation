#######################################
ADMINISTRATEUR TECHNIQUE ET FONCTIONNEL
#######################################

Description
===========

Ce profil permet de réaliser toutes les actions possibles de l'application.

.. DANGER::
   À utiliser avec précaution !

Fonctionnalités disponibles
===========================

Tableau de bord
---------------

.. image:: a_dashboard_admin.png

Widget *Recherche accès direct*
###############################

- Rechercher un dossier d'instruction par son identifiant

Widget *Les derniers dossiers consultés*
########################################

- Visualiser la liste des derniers dossiers consultés

Widget "Compteur de signatures électroniques"
#############################################

- afficher le nombre de signatures électroniques
