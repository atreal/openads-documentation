.. _parametrage_dossiers:

####################
Paramétrage dossiers
####################

Les dossiers
############


.. _parametrage_dossiers_dossier_autorisation_type:

===================================
Les types de dossier d'autorisation
===================================

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Type DA`)


Le principe
===========

Ce type de dossier d'autorisation peut également être appelé la série. C'est à
ce niveau que la numérotation des dossiers est gérée.
Lorsqu'un type de dossier d'autorisation est créé, modifié ou supprimé, la série
correspondante est créée, mise à jour ou supprimée.

Saisir un type de dossier d'autorisation
========================================

Les informations à saisir sont :

* **code** : c'est le code à deux ou trois caractères utilisé pour composer le numéro de
  dossier d'instruction (par exemple : PC).
* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'un type de dossier d'autorisation.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de le documenter.
* **confidentiel** : permet de modifier le comportement standard de la
  confidentialité. Un dossier qui possède cet attribut sera uniquement visible par les
  utilisateurs appartenant au même groupe que celui du dossier, et si ce groupe a accès
  aux dossiers confidentiels. Ce comportement permet par exemple de gérer la
  confidentialité des dossiers contentieux.
* **groupe** : les types de dossier d'autorisation sont liés à un groupe, qui définit
  quels utilisateurs ont le droit d'ajouter et consulter ces dossiers.
  (voir :ref:`parametrage_groupe`).
* **cacher le DA** : si cette option est activée, les dossiers d'autorisation de ce type
  ne seront pas visibles dans l'application.
* **Affichage des formulaires** : change l'affichage des formulaires des dossiers d'autorisation et d'instruction. Champ obligatoire.


.. _description_type_affichage_dossiers:

Les types d'affichages de dossier
=================================

L'affichage des formulaires sélectionné aura un impact sur l'affichage des dossiers d'instruction et des dossiers
d'autorisation.

Les types d'affichage disponibles sont :
  * **ADS** : affichage dédié aux dossiers d'instruction.
  * **CTX RE** : affichage dédié aux contentieux de type recours.

    .. image:: a_synthese_dossier_recours.png

  * **CTX IN** : affichage dédié aux contentieux de type infraction.

    .. image:: a_synthese_dossier_infraction.png

  * **DPC** : affichage dédié aux fonds de commerce.
  * **CONSULTATION ENTRANTE** : affichage des dossiers d'instruction pour les services consultés.
    Sur la synthèse des dossiers d'instruction, cet affichage reprend celui de type "ADS" en y
    ajoutant une partie concernant la consultation entrante.

    .. image:: a_synthese_consultation_entrante.png


.. _parametrage_dossiers_dossier_autorisation_type_detaille:

=============================================
Les types de dossier d'autorisation détaillés
=============================================

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Type DA Détaillé`)

Le principe
===========

Le type de dossier d'autorisation détaillé est utile pour faire la distinction
entre plusieurs cas d'utilisations d'un même type de dossier d'autorisation.
Par exemple, il existe deux catégories de type de dossier d'autorisation "Permis
de Construire", un spécifique pour les maisons individuelles et un pour les
autres constructions. Il existe un seul type de dossier d'autorisation
"Déclaration Préalable", il faut tout de même le saisir comme type de dossier
d'autorisation détaillé.

Saisir un type de dossier d'autorisation détaillé
=================================================

Les informations à saisir sont :

* **code** : c'est un code utilisé pour identifier rapidement le type de dossier
  d'autorisation détaillé. Généralement il se compose des deux ou trois caractères du
  type de dossier d'autorisation auquel on rajoute un ou plusieurs caractères
  spécifiques.
* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'un type de dossier d'autorisation détaillé.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de le documenter.
* **type de dossier d'autorisation** : (voir
  :ref:`parametrage_dossiers_dossier_autorisation_type`).
* **cerfa** : sélection du cerfa correspondant au dossier d'autorisation (voir :ref:`parametrage_dossiers_cerfa`).
* **cerfa pour lots** : sélection du cerfa correspondant aux lots du dossier d'autorisation (voir :ref:`parametrage_dossiers_cerfa`).
* **durée de validité** : durée de validité des dossiers d'autorisation (voir :ref:`parametrage_dossiers_incompletude`).
* **transmissible à plat'au** : permet d'indiquer si ce type de dossier est transmissible à Plat'AU ; il est possible de filtrer les types de DI grâce au paramètre :ref:`*dit_code__to_transmit__platau*<parametrage_parametre_identifiants>`.
* **couleur** : permet de sélectionner une couleur pour identifier les dossiers d'instruction de ce type.
* **Restreindre l’accès aux pièces sur le DA si le DI n’est pas clôturé (secret de l’instruction)** : Si cette case est cochée, l'affichage des pièces, sur les dossiers d'autorisation, sera restreint. Uniquement les utilisateurs ayant le droit *dossier_autorisation_secret_instruction* pourront visualiser et accéder aux pièces des dossiers d'instruction en cours.

.. _parametrage_dossiers_dossier_instruction_type:

==================================
Les types de dossier d'instruction
==================================

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Type DI`)

Le principe
===========

Le type de dossier d'instruction permet de déterminer pour chaque type de
dossier d'autorisation détaillé les différentes possibilités (initial,
modificatif, transfert, DOC, ...). Par exemple, sur le type de dossier
d'autorisation "Permis de Construire", il peut y avoir : un initial, un
modificatif, un transfert, une DOC et une DAACT alors que sur le type de dossier
d’autorisation "Déclaration d'Autorisation de Travaux", il peut y avoir : un
initial, une DAACT.

Saisir un type de dossier d'instruction
=======================================

Les informations à saisir sont :

* **code** : c'est un code utilisé pour identifier rapidement le type de dossier
  d'instruction. 
* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'un type de dossier d'instruction.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de le documenter.
* **sous-dossier** : case à cocher qui détermine si le dossier d'instruction est un
  sous dossier.
* **Sous-dossier pour le(s) DI** : détermine avec quel(s) type(s) de dossier d'instruction
  un sous dossier est compatible. Ce champs est visible uniquement si la case **sous-dossier**
  est coché.
* **type de dossier d'autorisation détaillé** : (voir
  :ref:`parametrage_dossiers_dossier_autorisation_type_detaille`) Ce champs n'est pas
  visible si la case **sous-dossier** est coché.
* **suffixe** : en décochant l'option on désactive le suffixe dans la numérotation du dossier.
  À n'utiliser que pour les types de DI initiaux afin de ne pas afficher le *P0*. Si la case
  **sous-dossier** est coché le suffixe doit obligatoirement être activé.
* **mouvement sitadel** : type de mouvement sitadel, seul les dossiers dont le 
  type de mouvement sitadel est défini seront exportés.
* **les mises à jour du dossier d'autorisation** : liste des actions de mise à jour du dossier
  d'autorisation possibles, propose les mises à jour de la localisation, des lots, des demandeurs,
  de l'état, des dates initiales, de la date de validité, de la date d'ouverture du chantier,
  de la date d'achèvement des travaux et des données techniques (CERFA).
  Si il s'agit d'un sous dossier ces actions ne doivent pas être sélectionnées.
* **Catégorie de tiers acteur** : Sélection des catégories dont les tiers pourront être
  ajoutés en tant qu'acteur du dossier. Ce champs n'est affiché que si l'option
  :ref:`option_parametrage_notif_auto_tiers<administration_liste_options>` est active.

Les champs suivants ne sont affichés que si l'option :ref:`option_parametrage_notif_auto_tiers<administration_liste_options>` est active.

* **Ajout automatique** : Catégories dont les tiers seront automatiquement ajoutés à la création d'un dossier du type paramétré.
* **Ajout par l'utilisateur** : Catégories dont les tiers pourront être ajoutés en tant qu'acteur d'un dossier du type paramétré.

.. _parametrage_dossiers_famille_travaux:

==================
Famille de travaux
==================

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Famille de travaux`)

Le principe
===========

Une famille de travaux comporte plusieurs natures de travaux.

Ce menu de paramétrage permet d'ajouter les familles de travaux qui vont regrouper les différentes natures de travaux.

Saisir une famille de travaux
=============================
* **code** : c'est un code utilisé pour identifier rapidement la famille de travaux. 
* **libellé** : texte à afficher dans l'interface lors de la sélection de la nature de travaux dans le DI.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de le documenter.
* **date de début de validité** : date de début de validité de la famille de travaux.
* **date de fin de validité** : date de fin de validité de la famille de travaux.

.. _parametrage_dossiers_nature_travaux:

=================
Nature de travaux
=================

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Nature de travaux`)

Le principe
===========

Une nature de travaux permet de faciliter l'identification des travaux sur les différents projets.
Il peut y avoir plusieurs natures de travaux pour une même famille de travaux, une nature de travaux a une seule famille de travaux.

Lors de la saisie on peut sélectionner avec quels types de dossier d'instruction la nature de travaux peut être compatible.

Saisir une nature de travaux
=============================
* **code** : c'est un code utilisé pour identifier rapidement la nature de travaux. 
* **libellé** : texte à afficher dans l'interface lors de la sélection de la nature de travaux dans le DI.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de le documenter.
* **famille de travaux** : la famille de travaux à laquelle la nature de travaux est liée.
* **type de dossier d'instruction** : permet d'indiquer avec quels type de dossier d'instruction la nature de travaux est compatible.
* **date de début de validité** : date de début de validité de la famille de travaux.
* **date de fin de validité** : date de fin de validité de la famille de travaux.

.. _parametrage_dossiers_contrainte:

===============
Les contraintes
===============

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Contrainte`)

Le principe
===========

Les contraintes peuvent être appliquées sur un dossier (voir :ref:`instruction_dossier_contrainte`). 
Certaines contraintes peuvent être ajoutées manuellement et d'autres sont récupérées depuis le SIG (voir 
:ref:`administration_synchronisation_contrainte`).

Saisir une contrainte
=====================

Les informations à saisir sont :

* **libellé** : le libellé de la contrainte.
* **nature** : la nature de la contrainte (POS ou PLU).
* **ordre d'affichage** : positionnement lors de l'affichage des contraintes.
* **groupe** : groupe de la contrainte.
* **sous-groupe** : sous-groupe de la contrainte.
* **texte** : texte de la contrainte.
* **présentée aux services consultés** : la contrainte est visible par les 
  services consultés.
* **date de début de validité** : date de début de validité de la contrainte.
* **date de fin de validité** : date de fin de validité de la contrainte.


.. _parametrage_enjeux:

==========
Les enjeux
==========

Le principe
===========

Des enjeux peuvent être appliqués sur un dossier. 
Pour ajouter un enjeu, il faut suivre le chemin suivant :

(:menuselection:`Paramétrage dossiers --> Dossiers --> Enjeu`)

Ce listing permet de visualiser les enjeux. Le '+' sur le listing permet d'ajouter un enjeu.

Une fois qu'un enjeu est ajouté, il peut être associé à un dossier d'instruction (voir :ref:`instruction_enjeux`).

Saisir un enjeu
===============

Les informations à saisir sont :

* **libellé** : le libellé de l'enjeu (champ obligatoire).
* **couleur** : couleur d'affichage de l'enjeu sur les dossiers.
* **priorité** : ordre d'importance des enjeux permettant de définir l'ordre d'affichage. Sur un dossier, les enjeux associés ayant une priorité plus élevée sont afffichés en premier. Un enjeu de priorité 0 sera affiché à la fin.
* **collectivité** : collectivités pour lesquelles l'enjeu pourra être utilisé.
* **type de dossier d'instruction** : types de dossier d'instruction pour lesquels les enjeux seront accessibles.


.. _parametrage_dossiers_demandes:

Les demandes
############

.. _parametrage_dossiers_demande_nature:

======================
Les natures de demande
======================

(:menuselection:`Paramétrage Dossiers --> Demandes --> Nature Demande`)

Le principe
===========

Chaque demande est reliée à une nature qui définit si la demande doit donner
lieu à la création d'un nouveau dossier d'autorisation ou si au contraire, elle
doit être rattachée à un dossier d'autorisation existant.

Saisir une nature de demande
============================

Les informations à saisir sont :

* **code** : c'est un code utilisé pour identifier rapidement la nature de la
  demande.
  Il faut utiliser "NOUV" si c'est pour une nouvelle demande et "EXIST" si c'est sur
  une demande existante.
* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'une nature de demande.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de documenter le paramétrage.


.. _parametrage_dossiers_demande_type:

====================
Les types de demande
====================

(:menuselection:`Paramétrage Dossiers --> Demandes --> Type Demande`)

Le principe
===========

Chaque demande est d'un type en particulier, cela permet de confitionner
lorsque celle-ci sera disponible dans les interfaces et les traitements
qui seront appliqués lors de sa création. Par exemple : la création ou non d'un
dossier d'instruction, le besoin de qualification.

Saisir un type de demande
=========================

Les informations à saisir sont :

* **code** : c'est un code utilisé pour identifier rapidement le type de la
  demande.
* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'un type de demande.
* **description** : c'est un texte qui n'est pas utilisé dans les autres
  interfaces de l'application, mais il permet de décrire l'élément de
  paramétrage en détail dans un objectif de le documenter.
* **groupe** : (voir :ref:`parametrage_groupe`).
* **type de dossier d'autorisation détaillé** : le type de dossier d'autorisation
  à créer si il doit en être créé un.
* **nature de la demande** : (voir :ref:`parametrage_dossiers_demande_nature`). Si
  le type de demande déclenche la création d'un sous dossier alors la demande doit
  obligatoirement être une demande sur existant.
* **états autorisés du dossier d'instruction ciblé** : état à partir du quel il est possible
  de créer la demande.
* **types compatibles des dossiers d’instruction en cours** : types des dossiers d'instructions pouvant être instruit en parallèle.
* **contraintes** : Liste des contraintes de récupération des demandeurs depuis le dossier d'autorisation :

  * *Récupération des demandeurs avec modification et ajout* : récupère les demandeurs du dossier d'autorisation et permet la modification des existants, ainsi que l'ajout de nouveau demandeur
  * *Récupération des demandeurs sans modification ni ajout* : récupère les demandeurs du dossier d'autorisation, ne permet pas la modification des existants ni l'ajout de nouveau demandeur
  * *Récupération des demandeurs sans modification avec ajout* : récupère les demandeurs du dossier d'autorisation, ne permet pas la modification des existants et permet l'ajout de nouveau demandeur
  * *Sans récupération des demandeurs* : ne récupère pas les demandeurs du dossier d'autorisation et permet l'ajout de nouveau demandeur

* **type de dossier d'instruction à créer** : le type de dossier d'instruction
  à créer si il doit en être créé un (initial, modificatif, daact, ...) (voir
  :ref:`parametrage_dossiers_dossier_instruction_type`).
* **qualification** : lors de la saisie d'une nouvelle demande, le dossier
  concerné par cette demande est marqué comme à qualifier si le type de demande
  est configuré comme nécessitant une qualification.
* **régénérer la clé d'accès au portail citoyen** : indique si cette demande implique ou non la régénération de la clé d'accès au portail citoyen associée au dossier (voir
  :ref:`portail_citoyen_regenerate_citizen_access_key_auto`).
* **événement** : c'est le type de l'événement d'instruction qui va être
  ajouté sur le dossier d'instruction au moment de la validation de la demande.
  Il est utilisé pour associer à la demande le bon récépissé, pour historiser
  la demande dans l'onglet "instruction" du dossier d'instruction et pour
  éventuellement modifier l'état du dossier d'instruction (voir
  :ref:`parametrage_dossiers_evenement`).
* **documents obligatoires** : liste des documents obligatoires à la saisie de la demande.
  Un document par ligne, si aucun document n'est saisi, aucun document ne sera demandé
  lors de la demande.

.. _parametrage_dossiers_cerfa:

Les CERFA
#########

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Cerfa`)

.. image:: m_parametrage_cerfa_form.png

Formulaire de configuration des Cerfa.

Les informations à saisir sont :

* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'un cerfa dans le formulaire d'édition des types détaillés de dossiers d'autorisation.
* **code** : c'est un code utilisé pour identifier rapidement le cerfa.
* **durée de validité** : durée de validité des cerfa.

.. note::

    Dans le formulaire, il suffit de cliquer sur le libellé du champ pour cocher
    la case correspondante.


.. _parametrage_dossiers_workflows:

Les workflows
#############

.. _parametrage_dossiers_evenement:

==============
Les événements
==============

(:menuselection:`Paramétrage Dossiers --> Workflows --> Événement`)

Le principe
===========

.. note::

    Il est nécessaire de distinguer deux éléments concernant l'événement. On
    parlera d'"événements d'instruction" pour tous les événements créés dans
    l'onglet "Instruction" du dossier d'instruction, et on parlera
    d'"événements" pour le paramétrage.


Le paramétrage de l'événement permet, lors de la création d'un événement
d'instruction, de :

* déclencher une action (recalcul d'informations du dossier d'instruction),
* modifier l'état du dossier d'instruction,
* générer un document PDF, lettre au pétitionnaire ou acte, au moyen d'une
  lettre type,
* déclencher des suivis de dates et des actions spécifiques pour les arrêtés.

Les événements d'instruction disponibles dans l'interface dépendent de l'état
dans lequel est le dossier d'instruction.

.. _parametrage_dossiers_saisir_evenement:

Saisir un événement
===================

Les informations à saisir sont :

* **libellé** : texte affiché dans l'interface lors du choix d'un événement à
  créer.
* **type** : permet de qualifier un type d'événement. Les valeurs disponibles
  sont : "arrêté" pour permettre une gestion propre aux arrêtés, "incomplétude" ou "majoration de délais" pour permettre certains calculs dans les tableaux de bord de l'instructeur et "affichage" permettant de ne pas compter l'instruction comme instruisant le dossier.
* **commentaire** : Permet de définir si un commentaire doit pouvoir être ajouté ou pas à l'événement d'instruction.
* **non verrouillable** : permet d'identifier un événement comme non 
  verrouillable. C'est-à-dire que l'événement d'instruction sera toujours
  modifiable même si le dossier d'autorisation est clôturé.
* **Non modifiable** : permet d'identifier un événement comme non modifiable.
  C'est-à-dire que l'événement d'instruction sera non modifiable après son ajout.
  Uniquement utilisable pour les événements sans lettre type, sinon un message d'erreur sera affiché. 
* **Non supprimable** : permet d'identifier un événement comme non supprimable.
  C'est-à-dire que l'événement d'instruction sera non supprimable après ajout.
* **Signataire obligatoire** : permet d'identifier qu'un signataire doit obligatoirement
  être associé à l'événement d'instruction pour qu'il puisse être finalisé.
* **Notification** : permet de choisir le type de notification des demandeurs voulu pour l'évènement.
* **Notification des services** : Définit si les services consultés pouront être notifiés.
* **Notification des tiers** : Définit si les tiers consultés pouront être notifiés et le mode de
  notification (automatique ou manuelle). Le mode de notification *automatique* n'est visible que si
  l'option :ref:`option_parametrage_notif_auto_tiers<administration_liste_options>` est active.
* **Tiers Destinataire** : paramètre la liste à choix des tiers destinaire sur les instructions. Les valeurs possibles sont :

  * *Aucun* : le sélécteur de tiers destinataire n'apparaît pas
  * *Acteur* : permet de sélectionner parmis la liste des acteurs du dossier d'instruction
  * *Acteur avec service consultant* : identique à *Acteur* et sélectionne par défaut le service consultant si celui-ci est définit

* **Désignation Opérateur** : case à cocher qui permet d'indiquer que l'action de désignation des opérateurs est disponible sur l'instruction.
* **type(s) d'habilitation des tiers consultés à notifier** : Champs visible uniquement si la
  notification automatique des tiers a été sélectionnée. Permet de choisir les types d'habilitation
  dont les tiers pourront être automatiquement notifiés.  
* **Contrôle de légalité par Plat'AU** : Permet d'identifier un événement dont l'instruction liée aura le suivi du contrôle de légalité transmis par Plat'AU.
* **retour** : permet de distinguer un événement "retour", c'est-à-dire un 
  événement qui ne sera utilsé qu'en tant qu'événement retour AR ou événement
  retour de signature. Ce champ n'est plus modifiable après validation du
  formulaire d'ajout.
* **état(s) source** : liste des états depuis lequel cet événement est
  disponible(voir :ref:`parametrage_dossiers_etat`).
* **type(s) de DI concerné(s)** : liste des types de dossier d'instruction pour
  lesquels cet événement est disponible (voir
  :ref:`parametrage_dossiers_dossier_instruction_type`).
* **restriction** : condition optionnelle permettant de refuser la validation du
  formulaire d'ajout d'événement d'instruction si le résultat est faux. Il est
  possible de vérifier deux conditions simultanément avec un OU logique ou un ET
  logique (cf. ci-après les types d'opérateurs et exemples).

  Champs utilisables : [archive_date_dernier_depot] [archive_date_complet]
  [archive_date_rejet] [archive_date_limite]
  [archive_date_notification_delai] [archive_date_decision]
  [archive_date_validite] [archive_date_achevement]
  [archive_date_conformite] [archive_date_chantier]
  [archive_date_limite_incompletude]
  [archive_delai_incompletude]
  [duree_validite] [delai]
  [delai_notification] [date_evenement]
  [duree_validite_parametrage][date_depot].

  Trois types d'opérateurs sont disponibles :

  * de comparaison : >=, <=, == et != ;
  * d'affectation : + et - ;
  * logiques : && et ||.

  Exemples :
  * date_evenement <= archive_date_dernier_depot + 1 mois
  * date_evenement <= archive_date_dernier_depot && archive_date_complet == date_depot

* **action** : c'est l'action déclenchée par cet événement. Les valeurs
  disponibles sont les valeurs du paramétrage des actions (voir
  :ref:`parametrage_dossiers_action`).
* **état** : paramètre disponible dans les règles de l'action. (voir
  :ref:`parametrage_dossiers_etat`).
* **délai** : paramètre disponible dans les règles de l'action. Ce champ peut être formulé en *jours* ou en *mois*.
* **tacite** : paramètre disponible dans les règles de l'action.
* **délai notification** : paramètre disponible dans les règles de l'action. Ce champ peut être formulé en *jours* ou en *mois*.
* **avis** : paramètre disponible dans les règles de l'action. Choix de l'avis
  correspondant à l'événement à utiliser dans les règles de l'action. Les
  valeurs disponibles sont les valeurs du paramétrage des avis (voir
  :ref:`parametrage_dossiers_avis_decision`).
* **autorité compétente** : paramètre disponible dans les règles de l'action, les valeurs disponibles sont paramétrables depuis les :ref:`autorités compétentes<parametrage_autorite_competente>`.
* **prise en compte métier** : paramètre disponible dans les règles de l'action, les valeurs disponibles sont paramétrables depuis les :ref:`prises en compte métier<parametrage_pec>`.
* **lettre type** : (voir :ref:`parametrage_dossiers_om_etat_lettretype`).
* **consultation** : cette case à cocher est présente pour afficher la liste
  des consultations dans le complément sous la forme « Vu l'avis » avis rendu
  « du service » nom du service consulté. Note: Les consultations marquées
  explicitement comme non-visibles ne sont pas affichées ici.
* **phase** : liste à choix des :ref:`phases <parametrage_phase>` à afficher sur la lettre recommandée.
* **Finaliser automatiquement** : cette case à cocher est présente pour activer la finalisation automatique de l'événement, donc la génération automatique de sa lettre type associée.
* **Type de documents** : Permet l'ajout d'un type de document qui sera associé à l'évènement courant. Lorsqu'une instruction d'évènement sera ajoutée à un dossier, le type de document sera enregistré. Si ensuite on modifie le type de document du même évènement, les instructions précédentes conserve le type de document précédent, et les nouvelles instructions d'évènement prendront en compte le nouveau type de document.
* **Au terme du délai** : événement déclenché automatiquement lorsque la
  date de tacite est dépassée.
* **Événement lors de la notification du correspondant** : événement déclenché par la saisie de la date de notification, si l'état du
  dossier d'instruction est bien lié à l'événement (état « compatible ») et si
  la restriction est valide. Cet événement peut donc déclencher un changement
  d'état du dossier d'instruction et une action.
* **Événement lors du retour de signature** : événement déclenché par la saisie de la date de retour de signature de l'autorité compétente de l'arrété.

.. _parametrage_dossiers_evenement_retour:

Les délais et délais de notification
====================================
Les champs **délai** et **délai notification** contiendront toutes les majorations attenantes à un événement.

Exemple : L'évenément **Accepter un dossier (avec réserves)** peut être paramétré ainsi dans le champ texte délai : 1 mois + 15 jours


Paramétrage d'un événement ayant un "événement lors de la notification du correspondant" ou un "événement lors du retour de signature"
======================================================================================================================================

Contexte : lorsqu'un événement déclenche un événement suite à la notification du correspondant ou suite au retour de signature, c'est, en général, cet événement lié qui effectue le
recalcul des dates. Pour cela il lui faut les mêmes paramètres que son
événement principal.

Dans un premier temps, il va falloir saisir l'événement qui se déclenche automatiquement. Pour cela, se reporter à la section :ref:`parametrage_dossiers_saisir_evenement`. 

N.B. : Il est important de cocher la case "retour" lors de la saisie de l'événement 
lié. C'est cette option qui va servir à distinguer cet événement des autres.

N.B. : Une fois la case "retour" cochée, les champs délai, accord tacite, délai 
notification, avis, événement retour AR et événement lors du retour de signature
ne seront plus modifiables.

Une fois le(s) événement(s) lié(s) saisie(s), l'événement principal (celui qui 
précéde l'événement suite à la notification ou au retour de signature) peut être saisi 
à son tour avec les bonnes règles de gestions :ref:`parametrage_dossiers_saisir_evenement`. 

N.B. : Dans la liste déroulante "événement lors de la notification du correspondant" et "événement lors du 
retour de signature", choisir les événements liés.

Une fois validé, le paramétrage saisie dans l'événement principal sera répercuté 
vers ses événements liés. 

Les paramètres répercutés de l'événement principal vers l'événement retour :

- le délai ;

- la décision tacite ;

- l'avis ;

- la restriction ;

- le délai de notification.

Un même événement ne peut pas être à la fois l'événement suite à la notification et suite à au retour de signature d'un événement principal et ne peut être utilisé que pour 
un seul événement.


Paramétrage des *modules d'évènement*
=====================================

*openADS* dispose d'un *système de modules* (pour l'instant dédié aux *évènements*) qui permet
d'enrichir l'application avec du code externe à celle-ci.

Un *module* pourra enrichir l'application de 3 manières différentes :

- **ajouter des « boutons »** (action de *portlet*) sur l'*instruction*, qui déclencheront un
  traitement avec un message ou afficheront des informations sur une nouvelle page ou dans une
  fenêtre modale;
- **modifier l'apparence** et les interactions d'une interface/page;
- **déclencher des traitements** à certains moments, soit pour modifier une interface (en modifiant
  un formulaire par exemple) soit pour modifier/stocker des informations calculées (ou récupérées
  depuis un service tiers par exemple).

Pour « activer » un *module* pour un *évènement* donné (qui sera chargé pour les *instructions*
correspondantes), un *administrateur* de l'application doit utiliser le nouvel onglet **Modules**,
et cliquer sur l'icône « + » pour ajouter le *module*.

Lors de l'ajout d'un *module* à un *évènement*, il est possible de renseigner différentes
informations le concernant:

- **Nom**: le *module* qui sera associé, choisi dans la liste de tous les
  *modules*
- **Déclencheur** (optionnel): un moment de déclenchement du *module* (notamment pour les *modules*
  de traitements)
- **Ordre** (optionnel): un ordre de déclenchement dans le cas d'un *module* de traitement ou bien
  un ordre d'affichage dans le cas où le *module* ajoute un/des « boutons »
- **Paramètres** (optionnel): des éléments de paramétrage du *module* (au format *INI*) pour
  personaliser le comportement du *module* (pour cette association avec cet *évènement*)

Il est possible d'ajouter plusieurs fois le même *module* à un *évènement*, mais uniquement si ses
paramètres sont différents.


.. _parametrage_dossiers_etat:

=========
Les états
=========

(:menuselection:`Paramétrage Dossiers --> Workflows --> État`)

Le principe
===========

Un état est la situation dans laquelle se trouve un dossier d'instruction à un
moment précis. Un dossier d'instruction est toujours dans un état. Cet état
existe dès la création du dossier d'instruction. Il va évoluer au cours de
l'instruction du dossier. C'est l'état du dossier d'instruction qui détermine
les événements possibles.

Saisir un état
==============

Les informations à saisir sont :

* **état** : c'est l'identifiant de l'état (dans le sens clé primaire de
  l'enregistrement), il est recommandé de saisir ici une chaine de caractères
  dans laquelle les espaces, les apostrophes ou tout caractère spécial sont
  remplacés par des "_", les caractères accentués par leur caractère non
  accentué et les majuscules remplacés par des minuscules (exemple : si le
  libellé de l'état est "Initialisé", la valeur à saisir ici serait
  "initialisé").
* **libellé** : texte à afficher dans l'interface lors de la sélection d'une
  état.
* **statut** : permet de catégoriser l'état pour permettre de gérer le statut du
  dossier "en cours" ou "clôturé".


.. _parametrage_dossiers_action:

===========
Les actions
===========

(:menuselection:`Paramétrage Dossiers --> Workflows --> Action`)

Le principe
===========

Une action permet de recalculer des informations du dossier d'instruction. Elle
est composée d'une série de règles de calculs. Chaque règle de calcul vise à
modifier la valeur du champ lié dans le dossier d'instruction.
Une action peut également mettre à jour la valeur de certaines données techniques (CERFA).

Elle accepte en paramètre de calcul :

* la valeur initiale de l'un des champs disponibles pour le dossier
  d'instruction,
* les valeurs du précédent dossier d'instruction (si ce n'est pas un 
  dépôt inital, exemple dans le cas d'une prorogation),
* des valeurs fixées dans le paramétrage de l'action,
* des valeurs fixées dans le paramétrage de l'événement déclenchant l'action,
* des formules de calcul,
* des valeurs de certaines données techniques (CERFA).

La valeur "null" vide la valeur du champ dans le Dossier d'Instruction.


Saisir une action
=================

Les informations à saisir sont :

* **action** : c'est l'identifiant de l'action (dans le sens clé primaire de
  l'enregistrement), il est recommandé de saisir ici une chaine de caractères
  dans laquelle les espaces, les apostrophes ou tout caractère spécial sont
  remplacés par des "_", les caractères accentués par leur caractère non
  accentué et les majuscules remplacés par des minuscules (exemple : si le
  libellé de l'action est "Prolonger le délai de validité", la valeur à saisir
  ici serait "prolonger_le_delai_de_validite").
* **libellé** : texte à afficher dans l'interface lors de la sélection
  d'une action.
* **pour tous les champs Règle** : règle rattaché au champ du dossier
  d'instruction du même nom à l'exception des règles sur données techniques (CERFA).
* les champs **règle de type DATE** attendent une composition de champs de fusion et de durées en *jours* ou en *mois*.
* les champs **règle de type BOOLEAN** attendent une composition de champs de fusion et valeurs booléens
* les champs **règle de type NUMERIQUE** attendent une composition de champs de fusion et valeurs numériques.
* les champs **règle de type UNIQUE** n'accepte qu'un seul élément et aucun opérateur +.
* **pour les 5 champs Règle données techniques (CERFA)** : dans le premier champ, saisir la donnée technique à modifier (choix restreint au données techniques (CERFA) présentées :ref:`ici <valeur_donnees_techniques>` ; dans le second, la valeur à lui affecter. Ce dernier peut contenir une ou plusieurs valeurs, issue(s) de celles présentées dans l'aide à la saisie. Dans le cas d'une composition, utiliser l'opérateur `+` pour concaténer les différentes valeurs.
* la **Règle type de dossier d'instruction** permet de changer le type de dossier d'intruction. La valeur de ce paramètre est un entier correspondant à l'ID du type que le dossier d'instruction doit avoir (ID à chercher dans Paramétrage dossiers > Type DI).
* la **Règle à qualifier** attend une valeur de type booléenne.
* la **Règle événement suivant tacite est incompletude** attend une valeur de type booléenne.

.. warning:: Pour la **Règle type de dossier d'instruction** il faut impérativement que l'**ID du type de dossier existe et soit du même type de dossier Autorisation**, et que ce soit pour le dossier d'Instruction initial du dossier d'Autorisation

Les champs disponibles pour la saisie des règles sont :

**Valeurs du dossier avant l'évènement**

[archive_etat] [archive_delai] [archive_accord_tacite] [archive_avis]
[archive_date_dernier_depot] [archive_date_complet] [archive_date_rejet] [archive_date_limite] [archive_date_notification_delai] [archive_date_decision] [archive_date_validite] [archive_date_achevement] [archive_date_conformite] [archive_date_chantier] [archive_etat_pendant_incompletude] [archive_date_limite_incompletude] [archive_delai_incompletude] [archive_autorite_competente] [archive_date_cloture_instruction] [archive_date_premiere_visite] [archive_date_derniere_visite] [archive_date_contradictoire] [archive_date_retour_contradictoire] [archive_date_ait] [archive_date_transmission_parquet] [archive_incompletude] [archive_incomplet_notifie] [duree_validite][date_depot][date_depot_mairie]

**Paramètres de l'évènement**

[etat] [delai] [accord_tacite] [avis_decision] [delai_notification] [date_evenement] [autorite_competente] [pec_metier] [complement_om_html] [complement2_om_html] [complement3_om_html] [complement4_om_html]

**Valeurs de l'événement d'instruction principal**

[date_envoi_signature] [date_retour_signature] [date_envoi_rar] [date_retour_rar] [date_envoi_rar] [date_retour_rar] [date_envoi_controle_legalite] [date_retour_controle_legalite]

**Paramètres du type detaillé du dossier d'autorisation**

[duree_validite_parametrage]

.. _valeur_donnees_techniques:

**Valeurs des données techniques (CERFA)**

[ctx_nature_travaux_infra_om_html] [ctx_article_non_resp_om_html]

**Suppression de la valeur**

[null]

**Valeurs pour les booléens**
[f] ou [false] [t] ou [true]

Exemples de règles :

* exemple avec 3 opérandes : date_evenement+delai+3mois
* exemple avec 2 opérandes : archive_date_complet+4jours
* exemple avec 1 opérande : null
* exemple de mise à jour de donnée technique (seule la concaténation est possibles


.. _parametrage_dossiers_incompletude:

========================
Gestion de la péremption
========================

Un dossier d'autorisation passera à l'état **Périmé** automatiquement grâce 
à une vérification périodique des conditions suivantes :

        * le DA est **accordé**,
        * la date de décision est renseignée,
        * le DI est **accepté**,
        * il n'y a ni **DOC** ni **DAACT** valide,
        * la date de validité du DA est inférieure à la date du jour.

=========================
Gestion de l'incomplétude
=========================

Pour les instructeurs, il y a deux problématiques distinctes : l'instruction des dossiers avec le suivi des dates et la gestion de l'incomplétude.
En cas d'incomplétude, les délais d'instruction sont suspendus. Par contre il peut y avoir des événements d'instruction, notamment concernant les prolongations de délais d'instruction.
Les événements d'incomplétude et de prolongation de délais ne sont pas activés dans un ordre déterminé : ils peuvent être activés par l'instructeur dès qu'il juge opportun de le faire.

Ainsi lorsque le dossier d'instruction est réputé incomplet notifié, les informations présentées sont celles spécifiques à l'incomplétude. L'instruction "principale" peut continuer et sera à nouveau présentée lorsque le dossier d'instruction sera considéré comme complet.

Voici la liste des champs spécifiques à l'incomplétude sur un dossier d'instruction, la plupart ne sont modifables que par l'intermédiaire d'une action d'instruction :
  * **incompletude** : booléen indiquant que le dossier d'instruction est considéré comme incomplet
  * **incomplet_notifie** : booléen indiquant que l'incomplétude est notifiée au demandeur, c'est cette information qui modifie l'affichage du dossier d'instruction
  * **delai_incompletude** : est utilisé pour le calcul de la date limite d'instruction de l'incomplétude
  * **date_limite_incompletude** : si **incomplet_notifie** est activé, remplace la date limite d'instruction
  * **evenement_suivant_tacite_incompletude** : si **incomplet_notifie** est activé, indique l'événement à appliquer en cas de dépassement de la date limite d'instruction

=================================
Gestion de la majoration de délai
=================================

Le principe
===========

Pour la consultation de certains services, l'instructeur a besoin de prolonger le
délai d'instruction.

Exemple de déroulement :

.. sidebar:: Note :

    État initial : les délais, date limite d'instruction, état et événement suivant tacite sont initialisés en fonction de l'action choisie pour ce type d'événement.

- dépôt de dossier PCI initial le 01/01/2013

    - délai d'instruction = 3 mois
    - date limite de complétude = date_depot + 1 mois
    - événement tacite = accord tacite
    - date limite d'instruction = date_depot + delai

- envoi d'un courrier de majoration de délai pour consultation ABF

    - type = majoration_delai
    - délai = 6 mois
    - événement suite à la notification = majoration_delai_abf_ar


- Retour de l'AR de majoration de délai consultation ABF

    - date limite d'instruction : archive_date_dernier_depot + delai
    - délai = archive_delai + 6 mois (6 mois est le délai de majoration_délai_abf)
    - événement suivant tacite = accord tacite


Configuration de la majoration
==============================

---------------------
Saisie des événements
---------------------

(:menuselection:`Paramétrage Dossiers --> Workflows --> Événements`)

- Majoration de délai :

    - type = majoration_delai
    - délai = 6 mois
    - accord tacite = oui
    - événement suite à la notification = majoration de délai après accusé de réception

- Majoration de délai après accusé de réception :

    - retour = oui
    - action = modifier le délai d'instruction
    - délai = 6 mois
    - accord tacite = oui
    - événement suivant tacite = accord tacite

- Accord tacite

    - action = accepter un dossier tacitement
    - état = accepté tacite
    - accord tacite = Oui
    - avis = accord tacite

------------------
Saisie des actions
------------------

(:menuselection:`Paramétrage Dossiers --> Workflows --> Action`)

- Modifier le délai d'instruction :

    - règle délai = delai
    - règle accord tacite = accord_tacite
    - règle date_limite = archive_date_dernier_depot + delai

- Accepter un dossier tacitement :

    - règle etat = etat
    - règle avis = avis_decision
    - règle date_validite = date_evenement + duree_validite
    - règle date_decision = date_evenement

.. _parametrage_dossiers_avis_decision:

========
Les avis
========

(:menuselection:`Paramétrage Dossiers --> Workflows --> Avis Décision`)

Le principe
===========

L'avis est un texte décrivant l'avis donné (par exemple "Favorable avec
réserves").

Saisir un avis
==============

Les informations à saisir sont :

* **libellé** : texte affiché dans l'interface lors du choix d'un avis.
* **type d'avis** : permet de catégoriser l'avis ("favorable", "défavorable" ou
  "annulation").
* **sitadel** : permet d'associer à cet avis un code pour les statistiques
  SITADEL.
* **sitadel_motif** : permet d'associer à cet avis un code pour les statistiques
  SITADEL.
* **tacite** : indique si c'est un avis tacite.
* **type d'avis de décision** : permet de catégoriser l'avis de décision avec un type dans une table de référence.
* **nature d'avis de décision** : permet de catégoriser l'avis de décision avec une nature dans une table de référence.
* **prescription** : indique s'il s'agit d'une prescription archéologique, ce champ n'est disponible seulement lorsque l'option globale :ref:`option_mode_service_consulte<parametrage_parametre>` est activée.

.. _parametrage_dossiers_avis_decision_type:

============================
Les types d'avis de décision
============================

(:menuselection:`Paramétrage Dossiers --> Workflows --> Type D'avis De Décision`)

Le principe
===========

Permet de catégoriser les avis de décision avec un type.

Saisir un type d'avis de décision
=================================

Les informations à saisir sont :

* **Code** : permet de codifier le type d'avis de décision.
* **Libellé** : texte affiché dans l'interface lors du choix d'un type d'avis de décision.
* **Description** : texte descriptif du type d'avis de décision.
* **Date de début de validité** : date de début de validité du type d'avis de décision.
* **Date de fin de validité** :  date de fin de validité du type d'avis de décision.

.. _parametrage_dossiers_avis_decision_nature:

==============================
Les natures d'avis de décision
==============================

(:menuselection:`Paramétrage Dossiers --> Workflows --> Nature D'avis De Décision`)

Le principe
===========

Permet de catégoriser les avis de décision avec une nature.

Saisir une nature d'avis de décision
====================================

Les informations à saisir sont :

* **Code** : permet de codifier la nature d'avis de décision.
* **Libellé** : texte affiché dans l'interface lors du choix d'une nature d'avis de décision.
* **Description** : texte descriptif de la nature d'avis de décision.
* **Date de début de validité** : date de début de validité de la nature d'avis de décision.
* **Date de fin de validité** :  date de fin de validité de la nature d'avis de décision.

.. _parametrage_dossiers_bible:

========
La bible
========

(:menuselection:`Paramétrage Dossiers --> Workflows --> Bible`)

Le principe
===========

La bible regroupe des phrases prédéfinies, qui permettent de remplir les :ref:`**compléments** d'instructions<instruction_complement>`.

Création d'une bible
====================

Les paramètres pour créer une bible sont :

* **libellé** : texte affiché dans l'interface lors du choix de bible.
* **événement** : l'événement d'instruction sur lequel la bible va s'appliquer
  si on ne le remplit pas alors il s'appliquera à tous les **événements**.
* **contenu** : le texte qui va être ajouté.
* **complement** : le numéro du complement visé (la valeur *tous* permet de rendre la bible disponible pour tous les compléments).
* **automatique** : permet d'ajouter cette bible directement via le bouton
  automatique sur l'**événement**.
* **Type de dossier d'autorisation** : le type de dossier d'autorisation pour lequel la bible va s'appliquer.
  Si on ne le remplit pas alors elle s'appliquera sur tous les types de dossier.
* **Collectivité** : collectivité pour laquelle la bible va s'appliquer.
  Si celle de niveau 2 est choisie alors elle s'appliquera sur toutes.
* **Pré-chargé** : permet d'ajouter cette bible directement lors de l'ajout de l'événement d'instruction.

.. image:: a_parametrage_bible.png

.. _parametrage_dossiers_editions:

Les éditions
############


.. _parametrage_dossiers_om_etat_lettretype:

==========================
Les états et lettres types
==========================

(:menuselection:`Paramétrage Dossiers --> Éditions --> État`)
(:menuselection:`Paramétrage Dossiers --> Éditions --> Lettre Type`)

Paramétrage des informations générales de l'édition
===================================================

.. image:: m_parametrage_etat_lettretype_edition.png

Les informations d'**édition** à saisir sont :

* **id** : identifiant de l'état/lettre type.
* **libellé** : libellé affiché dans l'application lors de la sélection d'une édition.
* **actif** : permet de définir si l'édition est active ou non.

.. note::

    Les champs **id** et **libellé** sont obligatoires, les **id** actif sont uniques.

Les champs de **paramètres généraux de l'édition** à saisir sont :

* **orientation** : orientation de l'édition (portrait/paysage).
* **format** : format de l'édition (A4/A3).
* **logo** : sélection du logo depuis la table des logos configurés.
* **logo haut/gauche** : position du coin haut/gauche du logo par rapport au coin
  haut/gauche de l'édition.
* **Marge gauche** : marge gauche de l'édition
* **Marge haut** : marge haute de l'édition
* **Marge droite** : marge droite de l'édition
* **Marge bas** : marge basse de l'édition

.. image:: m_parametrage_etat_lettretype_titre.png

Paramétrage du titre de l'édition.
==================================

* **titre** : éditeur riche permettant une mise en page complexe.

---------------------------------
Paramètres du titre de l'édition.
---------------------------------

Positionnement :

* **titre gauche** : positionnement du titre par rapport à la marge gauche de l'édition.
* **titre haut** : positionnement du titre par rapport à la marge haute de l'édition.
* **largeur de titre** : taille de la largeur du titre.
* **hauteur** : hauteur minimum du titre.

Bordure :

* **bordure** : Affichage ou non d'une bordure.

----------------------------------
Paramétrage du corps de l'édition.
----------------------------------

.. image:: m_parametrage_etat_lettretype_corps.png

* **corps** : éditeur riche permettant une mise en page complexe.

.. note::

    Il est possible d'ajouter les sous-états paramétrés via le menu **Insérer->
    Sous-états**, un sous-état de chaque type peut être affiché.

    Vous pouvez également transformer en code-barres une sélection en cliquant
    sur l'icône correspondante ; de la même façon il est possible de 
    mettre en majuscule une sélection (champ de fusion).

    Enfin, lorsque le curseur de saisie se situe dans un tableau, l'icône du
    fichier découpé permet de le rendre sécable/insécable.

----------------------------------------------
Paramétrage des champs de fusions de l'édition
----------------------------------------------

.. image:: m_parametrage_etat_lettretype_sql.png

* **SQL** : sélection d'un jeu de champs de fusion.

En fonction de la requête SQL sélectionnée, la liste des champs de fusion et des variables de remplacement sont affichés lors de la modification, ou de l'ajout, d'une édition.

Pour les lettres types spécifiquement, il existe également les *Données supplémentaires* qui peuvent être affichées et donc utilisées comme un champ de fusion dans l'édition.  
Ces données supplémentaires sont les données dynamiques pouvant être générées par les :ref:`modules<modules>`.

---------------------------------------
Paramétrage des sous-états de l'édition
---------------------------------------

.. image:: m_parametrage_etat_lettretype_sousetat.png

* **Police personnalisée** : sélection de la police des sous-états.
* **Couleur texte** : sélection de la couleur du texte des sous-états.

-------------------------------------
Paramétrage des tableaux des éditions
-------------------------------------
.. image:: m_parametrage_editon_tableau_creer.png

* **Créer un tableau** :

Choisir le nombre de lignes et de colonnes du tableau.

.. note::

    Il faut bien placer le curseur dans une des cellules du tableau que l'on 
    souhaite paramétrer.
    Idem pour le paramétrage des lignes et colonnes.

.. image:: m_parametrage_editon_tableau_menu_parametrage.png

.. image:: m_parametrage_editon_tableau_parametrage_generale.png

* **Paramétrage générale du tableau** :

    - Largeur :

    Ce champ sert à indiquer la largeur du tableau en % (UNIQUEMENT) par rapport 
    à la largeur du PDF.

    Par exemple, si le PDF fait une largeur de 30 cm et que la lageur du tableau
    est de 10%, le tableau fera 3 cm de largeur sur le PDF.

    - Hauteur :

    Ce champ sert à indiquer la hauteur du tableau en % (UNIQUEMENT) par rapport 
    à la hauteur du PDF.

    Par exemple, si le PDF fait une hauteur de 50 cm et que la hauteur du tableau
    est de 25%, le tableau fera 12.5 cm de hauteur sur le PDF.

    - Espacement inter-cellules :

    Espacement entre les cellules. En pixel.

    - Espace interne cellule :

    Espacement entre les bords de la cellule et son contenu. En pixel.

    - Bordure :

    Epaisseur des bordures du tableau. En pixel.

    - Titre :

    Lorsque cette case est cochée, elle permet de rajouter un titre au tableau.

    - Alignement :

    Permet de choisir le type d'alignement du texte dans le tableau. 
    Valeurs possibles : n/a (aucun), Gauche, Centré, Droite.

.. image:: m_parametrage_editon_tableau_suprimer.png

* **Supprimer un tableau**

.. image:: m_parametrage_editon_tableau_menu_parametrage_cellule.png

.. image:: m_parametrage_editon_tableau_parametrage_cellule.png

* **Paramétrage des cellules** :

    - Largeur :

    Ce champ sert à indiquer la largeur de la colonne en % (UNIQUEMENT) par 
    rapport à la largeur du tableau.

    Par exemple, si le tableau fait une largeur de 30 cm et que la largeur de la 
    colonne est de 10%, la colonne fera 3 cm de largeur.

    - Hauteur :

    Ce champ sert à indiquer la hauteur de la colonne en % (UNIQUEMENT) par 
    rapport à la hauteur du tableau.

    Par exemple, si le tableau fait une hauteur de 50 cm et que la hauteur de la
    colonne est de 25%, la colonne fera 12.5 cm de hauteur.

    - Type de cellule :

    Permet de définir si c'est une cellule "normale" ou une cellule qui va servir 
    d'en-tête dans le tableau.
    Valeurs possibles : Cellule, Cellule d'en-tête.

    - Étendue :

    Paramètre sur quoi doivent s'appliquer les paramètres renseignés.
    Valeurs possibles : n/a (aucun), Ligne, Colonne, Groupe de lignes, Groupe de 
    colonnes.

    - Alignement :

    Permet de choisir le type d'alignement du texte dans la cellule. 
    Valeurs possibles : n/a (aucun), Gauche, Centré, Droite.

.. image:: m_parametrage_editon_tableau_menu_fusionner.png

.. image:: m_parametrage_editon_tableau_fusionner.png

* **Fusionner des cellules** :

En sélectionnant les cellules à fusionner et en cliquant sur 
Tableau → Cellule → Fusionner les cellules les cellules seront fusionnées.

Si aucune cellule n'est sélectionnée, un menu apparaît :

    - Colonnes :

    Nombre de colonnes qui vont être fusionnées à partir de la cellule dans 
    laquelle le curseur est positionné.

    - Lignes :

    Nombre de lignes qui vont être fusionnées à partir de la cellule dans 
    laquelle le curseur est positionné.

.. image:: m_parametrage_editon_tableau_diviser.png

* **Diviser les cellules** :

Divise la cellule dans laquelle le curseur est positionné si elle avait été 
fusionnée avant.

.. image:: m_parametrage_editon_tableau_menu_ligne.png

.. image:: m_parametrage_editon_tableau_parametrage_ligne.png

* **Paramétrage des lignes** :

    - Type de ligne :

    Permlet de définir le type de la ligne.
    Valeurs possibles : En-tête, Corps, Pied.

    - Alignement :

    Permet de choisir le type d'alignement du texte dans la ligne. 
    Valeurs possibles : n/a (aucun), Gauche, Centré, Droite.

    - Hauteur : 

    Ce champ sert à indiquer la hauteur de la ligne en % (UNIQUEMENT) par 
    rapport à la hauteur du tableau.

    Par exemple, si le tableau fait une hauteur de 50 cm et que la hauteur de la
    ligne est de 25%, la ligne fera 12.5 cm de hauteur.

.. image:: m_parametrage_editon_tableau_inserer_ligne.png

* **Insérer une ligne** :

Permet d'insérer une ligne avant ou après la ligne sur laquelle le curseur est 
positionné.

.. image:: m_parametrage_editon_tableau_effacer_ligne.png

* **Éffacer une ligne** :

Supprimer la ligne sur laquelle le curseur est positionné.

.. image:: m_parametrage_editon_tableau_couper_ligne.png

* **Couper une ligne** :

Coupe la ligne sur laquelle le curseur est positionné.

.. image:: m_parametrage_editon_tableau_copier_ligne.png

* **Copier une ligne** :

Copie la ligne sur laquelle le curseur est positionné.

.. image:: m_parametrage_editon_tableau_coller_ligne.png

* **Coller une ligne** :

Colle la ligne qui avait été copiée/coupée avant ou après la ligne sur laquelle 
le curseur est positionné.

.. image:: m_parametrage_editon_tableau_inserer_colonne.png

* **Insérer une colonne** :

Insère une colonne avant ou après la colonne sur laquelle le curseur est 
positionné.

.. image:: m_parametrage_editon_tableau_effacer_colonne.png

* **Effacer une colonne** :

Supprime la colonne sur laquelle le curseur est positionné.

----------------------------------------
Paramétrage des code-barres des éditions
----------------------------------------

.. image:: m_parametrage_editon_codebarres_initial.png

Saisir le champ de fusion

.. image:: m_parametrage_editon_codebarres_select.png

Sélectionner le champ de fusion

.. image:: m_parametrage_editon_codebarres_ajout.png

Cliquer sur le bouton de génération du code-barres puis valider le formulaire 
pour enregistrer les changements

--------------------------------------------
Ajout de paramètre spécifique dans l'édition
--------------------------------------------
Il est possible d'ajouter des paramètres spécifques dans les éditions.
Pour cela, il faut ajouter un paramètre à l'application.

(:menuselection:`Administration --> Paramètre`)

.. sidebar:: Note :

    Le paramètre ne doit pas commencer par option\_, ged\_, erp\_, sig\_ ou id\_.

Afin que le paramètre s'affiche dans l'édition, il faut l'ajouter précédé d'un &.

Par exemple, le paramètre se nommant mail\_contact s'utilisera comme ceci :

.. image:: m_parametrage_editon_parametre.png


.. _parametrage_dossiers_marqueur_signature:

------------------------------------------------------------
Ajout d'un marqueur d'emplacement de la signature manuscrite
------------------------------------------------------------

Dans l'édition qui génèrera un document envoyé en signature (via connecteur parapheur), il est possible d'ajouter le texte **{{signature_placeholder}}** afin d'indiquer au service de signature de document où incruster l'image de la signature manuscrite du signataire (souvent accompagnée d'un texte du type "Signé le XX/XX/XXXX par XXX)".

.. sidebar:: Astuce :

   Si vous ne souhaitez pas que le texte "{{signature_placeholder}}" apparaisse, nous vous invitons à **changer la couleur de la police en blanc** (ou de la même couleur que le fond du document à cet endroit).

Il y a néanmoins certaines conditions à respecter pour que cela fonctionne :

* que le texte *{{signature_placeholder}}* soit **seul sur la ligne** (pas d'autre texte)
* que le positionnement de ce texte ne soit pas fait avec des espaces à gauche de celui-ci; utilsez l'**alignement centré ou à droite avec des espaces à droite** si besoin.
* qu'il y ait **suffisamment de lignes blanches sous la ligne contenant ce texte** pour laisser de l'espace à l'incrustation de l'image.

.. note::

   Nous recommendons de sauter au moins 5 lignes après le texte *{{signature_placeholder}}*

Après avoir envoyé votre document en signature, il apparaitra signé avec l'image de la signature manuscrite à l'endroit où le texte *{{signature_placeholder}}* a été placé.


---------------------------------------------
Paramétrage des contraintes dans les éditions
---------------------------------------------

Pour afficher les contraintes du dossier, il faut saisir une variable de remplacement :

* **&contraintes** : Affiche toutes les contraintes.

* **&contraintes(liste_groupe=g1,g2...;liste_ssgroupe=sg1,sg2...;service_consulte=t;affichage_sans_arborescence=t)** : Toutes les options sont optionnelles. Les options liste_groupe et liste_ssgroupe peuvent contenir une valeur unique ou plusieurs valeurs separées par une virgule, sans espace. Chaque valeur est un texte qui va être recherché dans le nom de groupe ou de sous-groupe en fonction du critère.

Par exemple :

- &contrainte :
    liste de toutes les contraintes du dossier.

- &contrainte(groupe=zonage,servitudes)
    liste de toutes les contraintes de groupe 'zonage' ou 'servitudes'.

L'option service_consulte permet d'ajouter une condition sur le champ du même nom. Elle peut prendre t (Oui) ou f (Non) comme valeur.

La dernière option affichage_sans_arborescence permet d'avoir la liste des contraintes sans affichage des groupes, sous-groupes, et puces. Elle peut prendre t (Oui) ou f (Non) comme valeur.

=========
Les logos
=========

(:menuselection:`Paramétrage Dossiers --> Éditions --> Logo`)

Le principe
===========

Ce menu permet de paramétrer les logos affichés sur les éditions.
Dans le cas d'un paramétrage multi-commune, les logos de chaque commune, ainsi que celui de la communauté, doivent avoir le même identifiant (**Id**). Cela permet aux éditions d'un dossier d'instruction de récupèrer le logo de la collectivité.

Paramétrage d'un logo
=====================

.. image:: a_parametrage_edition_logo.png

Les informations d'un **logo** sont :

* **Id** : Identifiant du logo.
* **Libellé** : Texte affiché quand le logo est appliqué à une édition.
* **Description** : Description du logo affichée lors de la sélection d'un logo depuis une lettre type ou un état.
* **Fichier** : Sélection du fichier image contenant le logo.
* **Résolution** : Résolution de l'image lors de son affichage sur une édition.
* **Actif** : Rend le logo disponible ou non depuis les lettres types et états.


Copie d'un logo
===============

.. image:: a_parametrage_edition_logo_portlet.png

L'action **copier** disponible depuis le portlet d'actions contextuelles de chaque logo, permet de dupliquer un logo afin de pré-remplir les différents champs de celui-ci.
